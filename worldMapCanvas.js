const canvas = document.getElementById('canvas');
const svg = d3.select(canvas).append('svg')
const width = canvas.clientWidth;
const height = canvas.clientHeight;


svg.attr('width', width)
svg.attr('height', height)
svg.attr('id', 'svg')
svg.attr("viewBox", [0, 0, width, height])

// keep track on mouse position for tooltip positioning
let cursor_x = -1;
let cursor_y = -1;
document.onmousemove = function (event) {
    cursor_x = event.pageX;
    cursor_y = event.pageY;
}

// zoom + pan inspired by https://bl.ocks.org/puzzler10/91a6b53d4237c97752d0e466443dad0b
var zoom_handler = d3.zoom()
    .scaleExtent([0.8, 16])
    .on("zoom", zoom_actions);

function zoom_actions() {
    dataRegion.attr("transform", d3.event.transform);
    // hide tooltip
    document.getElementById('tooltip').style.visibility = "hidden"
}

zoom_handler(svg);

let dataRegion;

// filter dataset to only contain Week 51's Results
var filteredEuData = euDataSetRaw.records.filter(datum => datum.year_week == "2020-51");

// colorscale
var colorScale = d3.scalePow()
    .exponent(0.6)
    .domain([0, 700])
    .range(["white", "darkred"])

// function to calculate incidence per 100.000 inhabitants
function incidence(country) {
    return country.cases_weekly / (country.popData2019 / 100000);
}


function update(projection) {
    // clear svg because updating would be more complicated
    document.getElementById('svg').innerHTML = "";
    // hide tooltip
    document.getElementById('tooltip').style.visibility = "hidden"

    projection.fitExtent([[0, 0], [canvas.clientWidth, canvas.clientHeight]], geoJsonRaw)

    dataRegion = svg.append('g')

    var geoGenerator = d3.geoPath()
        .projection(projection)


    var geoPaths = dataRegion.selectAll('path')
        .data(geoJsonRaw.features)
    geoPaths.enter()
        .append('path')
        .attr('d', geoGenerator)
        .style('fill', (d) => {
            country = filteredEuData.filter(datum => datum.countryterritoryCode == d.id)[0];
            if (country) { // not all countries in the geojson are included in the eu data
                incidenceNumber = incidence(country);
                return colorScale(incidenceNumber);
            } else return "lightblue";})
        .style('stroke', 'black')
        .style('stroke-width', () => {console.log("Werner"); return "0.5px"})
        .on("click", function (d, i) {

            let country = filteredEuData.filter(datum => datum.countryterritoryCode == d.id)[0];
            // check for no data
            if (country) {
                document.getElementById('tooletipCountryName').textContent = country.countriesAndTerritories.replaceAll("_", " ");
                document.getElementById('tooltip_casesPerWeek').textContent = country.cases_weekly;
                document.getElementById('tooltip_deathsPerWeek').textContent = country.deaths_weekly;
                document.getElementById('tooltip_incidence').textContent = (Math.round(incidence(country) * 100) / 100).toString();
                document.getElementById('tooltip_population').textContent = d3.format(",")(country.popData2019).replaceAll(",", " ");
            } else {
                document.getElementById('tooletipCountryName').textContent = d.properties.name;
                document.getElementById('tooltip_casesPerWeek').textContent = "-";
                document.getElementById('tooltip_deathsPerWeek').textContent = "-";
                document.getElementById('tooltip_incidence').textContent = "-";
                document.getElementById('tooltip_population').textContent = "-";
            }
            // positioning of tooltip
            tooltip = document.getElementById('tooltip');
            tooltip.style.visibility = 'visible';
            tooltip.style.top = (cursor_y - tooltip.clientHeight) + "px";
            tooltip.style.left = (cursor_x - tooltip.clientWidth) + "px";

        })
}

update(d3.geoMercator());
